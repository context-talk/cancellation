package main

import (
	"context"
	"log"
	"net/http"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// Send both requests
	go sendRequest(ctx, cancel, "http://google.com")
	go sendRequest(ctx, cancel, "http://duckduckgo.com")

	time.Sleep(5 *time.Second)
}

// sendRequest will send a GET request to the specified URL and call the cancel
// function as soon as it's done.
func sendRequest(ctx context.Context, cancel context.CancelFunc, url string)  {
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		log.Fatalf("Failed to set up request for %q: %v", url, err)
	}

	log.Printf("Sending request to %q", url)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
	    log.Fatalf("Failed to send request for %q: %v", url, err)
	}
	defer resp.Body.Close()

	log.Printf("Received %d from %s", resp.StatusCode, url)
}
