# Cancellation

Examples on how to use the [context](https://pkg.go.dev/context?tab=doc)
package for cancellation in multiple scenarios:

- Concurrent code
- HTTP requests
- Command line applications
