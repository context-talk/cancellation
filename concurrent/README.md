# Concurrent

An example of how to use [context](https://pkg.go.dev/context?tab=doc)
to cancel work when dealing with concurrency.
