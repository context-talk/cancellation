package main

import (
	"context"
	"log"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	go expensiveWorker(ctx)

	time.Sleep(5 * time.Second)
	cancel()
	time.Sleep(time.Second)
}

func expensiveWorker(ctx context.Context) {
	for {
		select {
		case <-time.Tick(time.Second):
			log.Print("tick")
		case <-ctx.Done():
			log.Fatalf("done")
		}
	}
}
